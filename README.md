# TCB-Teardown-test
Evade SNI filtering!
```
    TCP Peer A               middlebox                           TCP Peer B

1.  CLOSED                                                        LISTEN

2.  SYN-SENT          -->    <SEQ=100><CTL=SYN>               --> SYN-RECEIVED

3.  ESTABLISHED       <--    <SEQ=300><ACK=101><CTL=SYN,ACK>  <-- SYN-RECEIVED

4.  ESTABLISHED       -->    <SEQ=101><ACK=301><CTL=ACK>      --> ESTABLISHED

5.  TTL-limited-RST-sent --> <SEQ=101><ACK=301><CTL=RST,ACK>
                             stop tracking this connection

6.  ESTABLISHED       -->    <SEQ=101><ACK=301><CTL=ACK><DATA> --> ESTABLISHED
```

# requirements
* linux
* CAP_NET_RAW.
# Usage
```
cd raw-test
cargo build
cd target/debug
sudo setcap CAP_NET_RAW=ep raw-test
./raw-test 10 true 2
```
raw-test [default TTL] [enable TTL guessing] [delta]
\
More information about arguments: [raw-test/src/main.rs:11](https://codeberg.org/Spilopelia-chinensis/TCB-Teardown-test/src/branch/main/raw-test/src/main.rs#L11)