const loopback_ipv6:[u8;16]=[
	0,0,0,0,
	0,0,0,0,
	0,0,0,0,
	0,0,0,1
];
pub fn read_u16_be(s:&[u8], offset:usize)->u16{
	return (s[offset] as u16) << 8 | (s[offset+1] as u16);
}
pub fn write_u16_be(s:&mut [u8], value:u16, offset:usize){
	let t=value.to_be_bytes();
	s[offset]=t[0];
	s[offset+1]=t[1];
}
pub fn read_u32_be(s:&[u8], offset:usize)->u32{
	return (s[offset] as u32) << 24 | (s[offset+1] as u32) <<16 | (s[offset+2] as u32) << 8 | (s[offset+3] as u32);
}
pub fn write_u32_be(s:&mut [u8], value:u32, offset:usize){
	let t=value.to_be_bytes();
	for i in 0..4{
		s[offset+i]=t[i];
	}
}

pub fn get_version(h:&[u8])->u8{
	h[0]>>4
}
pub fn set_version(h:&mut [u8], version:u8){
	h[0] = (h[0]&0xf) | (version<<4);
}

pub fn ipv6_get_payload_length(h:&[u8])->u16{
	read_u16_be(h,4)
}
pub fn ipv6_set_payload_length(h:&mut [u8], l:u16){
	write_u16_be(h,l,4);
}
pub fn ipv6_get_next_header(h:&[u8])->u8{
	h[6]
}
pub fn ipv6_set_next_header(h:&mut [u8], n:u8){
	h[6]=n;
}
pub fn ipv6_get_hop_limit(h:&[u8])->u8{
	h[7]
}
pub fn ipv6_set_hop_limit(h:&mut [u8], n:u8){
	h[7]=n;
}
pub fn ipv6_get_saddr(h:&[u8])->&[u8]{
	&h[8..24]
}
pub fn ipv6_set_saddr(h:&mut [u8], addr:&[u8]){
	h[8..24].clone_from_slice(&addr[0..16]);
}
pub fn ipv6_get_daddr(h:&[u8])->&[u8]{
	&h[24..40]
}
pub fn ipv6_set_daddr(h:&mut [u8], addr:&[u8]){
	h[24..40].clone_from_slice(&addr[0..16]);
}
pub fn get_nextheader_headersize(packet:&[u8])->(u8,usize){
	let mut headerSize:usize=40;
	let mut nextHeader=packet[6];
	while packet.len() >= headerSize+8{
		match nextHeader{
			//IPv6 Hop-by-Hop Option
			//Routing Header for IPv6
			//Destination Options for IPv6
			0|43|60=>{
				nextHeader=packet[headerSize];
				headerSize += 8 + (packet[headerSize+1] as usize) * 8;
			},
			//Fragment Header
			44=>{
				nextHeader=packet[headerSize];
				headerSize+=8;
			},
			_=>break
		}
	}
	return (nextHeader,headerSize)
}

pub fn ipv4_get_IHL(h:&[u8])->u8{
	(h[0]&0xf)*4
}
pub fn ipv4_set_IHL(h:&mut [u8], n:u8){
	h[0] = (h[0]&0xf0) | (n/4)
}
pub fn ipv4_get_total_length(h:&[u8])->u16{
	read_u16_be(h,2)
}
pub fn ipv4_set_total_length(h:&mut [u8], n:u16){
	write_u16_be(h,n,2);
}
pub fn ipv4_get_TTL(h:&[u8])->u8{
	h[8]
}
pub fn ipv4_set_TTL(h:&mut [u8], n:u8){
	h[8]=n
}
pub fn ipv4_get_protocol(h:&[u8])->u8{
	h[9]
}
pub fn ipv4_set_protocol(h:&mut [u8], p:u8){
	h[9]=p;
}
pub fn ipv4_get_checksum(h:&[u8])->u16{
	read_u16_be(h,10)
}
pub fn ipv4_set_checksum(h:&mut [u8], c:u16){
	write_u16_be(h,c,10);
}
pub fn ipv4_get_saddr(h:&[u8])->&[u8]{
	&h[12..16]
}
pub fn ipv4_set_saddr(h:&mut [u8], addr:&[u8]){
	h[12..16].clone_from_slice(&addr[0..4]);
}
pub fn ipv4_get_daddr(h:&[u8])->&[u8]{
	&h[16..20]
}
pub fn ipv4_set_daddr(h:&mut [u8], addr:&[u8]){
	h[16..20].clone_from_slice(&addr[0..4]);
}
pub fn tcp_get_sport(h:&[u8])->u16{
	read_u16_be(h,0)
}
pub fn tcp_set_sport(h:&mut [u8], port:u16){
	write_u16_be(h,port,0);
}
pub fn tcp_get_dport(h:&[u8])->u16{
	read_u16_be(h,2)
}
pub fn tcp_set_dport(h:&mut [u8], port:u16){
	write_u16_be(h,port,2);
}
pub fn tcp_get_seq(h:&[u8])->u32{
	read_u32_be(h,4)
}
pub fn tcp_set_seq(h:&mut [u8], seq:u32){
	write_u32_be(h,seq,4);
}
pub fn tcp_get_ack(h:&[u8])->u32{
	read_u32_be(h,8)
}
pub fn tcp_set_ack(h:&mut [u8], ack:u32){
	write_u32_be(h,ack,8);
}
pub fn tcp_get_data_offset(h:&[u8])->u8{
	(h[12]>>4)*4
}
pub fn tcp_set_data_offset(h:&mut [u8], n:u8){
	h[12] = (n/4)<<4;
}
pub fn tcp_get_flags(h:&[u8])->u8{
	h[13]
}
pub fn tcp_set_flags(h:&mut [u8], flags:u8){
	h[13]=flags;
}
pub fn tcp_set_window(h:&mut [u8], n:u16){
	write_u16_be(h,n,14);
}
pub fn tcp_get_checksum(h:&[u8])->u16{
	read_u16_be(h,16)
}
pub fn tcp_set_checksum(h:&mut [u8], c:u16){
	write_u16_be(h,c,16);
}

fn clear_high16(n:u32)->u32{
	let mut t=n;
	while t>0xffff{
		t=(t>>16)+(t&0xffff);
	}
	return t;
}
fn u16_sum(data:&[u8])->u32{
	let mut t=0u32;
	let mut i=0;
	while i < data.len()-1{
		t+=read_u16_be(data,i) as u32;
		i+=2;
	}
	if data.len()%2!=0{
		t+=(data[data.len()-1] as u32)<<8;
	}
	t
}
pub fn ipv4_checksum(header:&mut [u8]){
	let t=clear_high16(u16_sum(header));
	ipv4_set_checksum(header,0xffff-t as u16);
}
pub fn tcp4_checksum(sAddr:&[u8], dAddr:&[u8], tcpPacket:&[u8])->u16{
	let mut pseudoHeader=[0u8;12];
	pseudoHeader[0..4].clone_from_slice(&sAddr[0..4]);
	pseudoHeader[4..8].clone_from_slice(&dAddr[0..4]);
	pseudoHeader[9]=6;
	write_u16_be(&mut pseudoHeader,tcpPacket.len() as u16,10);
	
	let t=u16_sum(&pseudoHeader[..])+u16_sum(tcpPacket);
	0xffff-clear_high16(t) as u16
}
pub fn tcp6_checksum(sAddr:&[u8], dAddr:&[u8], tcpPacket:&[u8])->u16{
	let mut pseudoHeader=[0u8;40];
	pseudoHeader[0..16].clone_from_slice(&sAddr[0..16]);
	pseudoHeader[16..32].clone_from_slice(&dAddr[0..16]);
	write_u32_be(&mut pseudoHeader,tcpPacket.len() as u32,32);
	pseudoHeader[39]=6;
	
	let t=u16_sum(&pseudoHeader[..])+u16_sum(tcpPacket);
	0xffff-clear_high16(t) as u16
}

pub fn filter<A,T,F1,F2>(packet:&[u8], sSockAddr:&A, defaultTTL:u8, gen_hop:F1, sock:&T, send_to:F2)->bool
where
	F1:Fn(u8,u8)->u8,
	F2:Fn(&T,&[u8],i32,&A)->Result<usize,std::io::Error>,
{
	match packet[0] >> 4{
		//IPv6
		6=>{
			//minimum size of IPv6 header is 40 bytes, minimum size of TCP header is 20 bytes
			if packet.len() < 60{
				return false;
			}
			let (nextHeader,headerSize) = get_nextheader_headersize(packet);
			if nextHeader != 6{
				return false;
			}
			if packet.len() < headerSize+20{
				return false;
			}
			//SYN+ACK
			if packet[headerSize+13]&0b00010010 == 0b00010010{
				let sAddr=ipv6_get_saddr(packet);
				if sAddr[0]&0b11111110 == 0xfc{//ignore fc00::/7
					return false;
				}else if *sAddr==loopback_ipv6{//ignore ::1/128
					return false;
				}
				let dAddr=ipv6_get_daddr(packet);
				if sAddr[0..8]==dAddr[0..8]{//check if sAddr and dAddr have the same prefix
					return false;
				}
				let resTTL=ipv6_get_hop_limit(packet);
				
				let sPort=tcp_get_sport(&packet[headerSize..]);
				let dPort=tcp_get_dport(&packet[headerSize..]);
				let seq=tcp_get_seq(&packet[headerSize..]);
				let ack=tcp_get_ack(&packet[headerSize..]);
				
				let mut chksum=0u16;
				let mut buf=[0u8;60];
				
				//complete basic three-way handshake
				set_version(&mut buf[0..40],6);
				ipv6_set_payload_length(&mut buf[0..40],20);
				ipv6_set_next_header(&mut buf[0..40],6);
				ipv6_set_hop_limit(&mut buf[0..40],gen_hop(defaultTTL,resTTL));
				ipv6_set_saddr(&mut buf[0..40],dAddr);
				ipv6_set_daddr(&mut buf[0..40],sAddr);
				
				tcp_set_sport(&mut buf[40..60],dPort);
				tcp_set_dport(&mut buf[40..60],sPort);
				tcp_set_seq(&mut buf[40..60],ack);
				tcp_set_ack(&mut buf[40..60],seq+1);
				tcp_set_data_offset(&mut buf[40..60],20);
				tcp_set_flags(&mut buf[40..60],0b00010000);//ACK
				tcp_set_window(&mut buf[40..60],65535);
				
				chksum=tcp6_checksum(dAddr,sAddr,&buf[40..60]);
				tcp_set_checksum(&mut buf[40..60],chksum);
				send_to(sock,&buf[..],0,sSockAddr).unwrap();
				
				//send a reset
				tcp_set_flags(&mut buf[40..60],0b00010100);//RST+ACK
				
				tcp_set_checksum(&mut buf[40..60],0);
				chksum=tcp6_checksum(dAddr,sAddr,&buf[40..60]);
				tcp_set_checksum(&mut buf[40..60],chksum);
				send_to(sock,&buf[..],0,sSockAddr).unwrap();
				
				return true;
			}else{
				return false;
			}
		},
		//IPv4
		4=>{
			//minimum size of IPv4 header is 20 bytes, minimum size of TCP header is 20 bytes
			if packet.len() < 40{
				return false;
			}
			//Protocol
			if packet[9] != 6{
				return false;
			}
			let headerSize=((packet[0]&0x0f) as usize)*4;
			if packet.len() < headerSize+20{
				return false;
			}
			//SYN+ACK
			if packet[headerSize+13]&0b00010010 == 0b00010010{
				let sAddr=ipv4_get_saddr(packet);
				if sAddr[0]==127{//ignore 127.0.0.1/8
					return false;
				}else if sAddr[0]==10{//ignore 10.0.0.0/8
					return false;
				}
				let prefix1=read_u16_be(sAddr,0);
				if prefix1==0xc0a8{//ignore 192.168.0.0/16
					return false;
				}else if prefix1&0xfff0 == 0xac10{//ignore 172.16.0.0/12
					return false;
				}
				let dAddr=ipv4_get_daddr(packet);
				let resTTL=ipv4_get_TTL(packet);
				
				let sPort=tcp_get_sport(&packet[headerSize..]);
				let dPort=tcp_get_dport(&packet[headerSize..]);
				let seq=tcp_get_seq(&packet[headerSize..]);
				let ack=tcp_get_ack(&packet[headerSize..]);
				
				let mut chksum=0u16;
				let mut buf=[0u8;40];
				
				//complete basic three-way handshake
				buf[0]=0x45;
				ipv4_set_total_length(&mut buf[0..20],40);
				ipv4_set_TTL(&mut buf[0..20],gen_hop(defaultTTL,resTTL));
				ipv4_set_protocol(&mut buf[0..20],6);
				ipv4_set_saddr(&mut buf[0..20],dAddr);
				ipv4_set_daddr(&mut buf[0..20],sAddr);
				
				tcp_set_sport(&mut buf[20..40],dPort);
				tcp_set_dport(&mut buf[20..40],sPort);
				tcp_set_seq(&mut buf[20..40],ack);
				tcp_set_ack(&mut buf[20..40],seq+1);
				tcp_set_data_offset(&mut buf[20..40],20);
				tcp_set_flags(&mut buf[20..40],0b00010000);//ACK
				tcp_set_window(&mut buf[20..40],65535);
				
				ipv4_checksum(&mut buf[0..20]);
				chksum=tcp4_checksum(dAddr,sAddr,&buf[20..40]);
				tcp_set_checksum(&mut buf[20..40],chksum);
				send_to(sock,&buf[..],0,sSockAddr).unwrap();
				
				//send a reset
				tcp_set_flags(&mut buf[20..40],0b00010100);//RST+ACK
				
				tcp_set_checksum(&mut buf[20..40],0);
				chksum=tcp4_checksum(dAddr,sAddr,&buf[20..40]);
				tcp_set_checksum(&mut buf[20..40],chksum);
				send_to(sock,&buf[..],0,sSockAddr).unwrap();
				
				return true;
			}else{
				return false;
			}
		},
		_=>false
	}
}
